using System;
using Moonin.Input;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;
using Moonin.Extensions;

namespace Funko
{
    public class Game : MonoBehaviour
    {
        [SerializeField] private CameraController _camera;
        [SerializeField] private PlayerController _player;
        [SerializeField] private Level _level;

        private bool _began;
        
        private void Start() => Setup();
        public void Setup()
        {
            _camera.SetFovWidth(FunkoSettingsRef.Value.LevelWidth);
            _camera.SetTarget(_player.transform);
            
            _level.Setup(_player.transform, _camera.ViewHeight);
            
            LeanInput.FingerDownStream.Subscribe(finger => OnJump(ToScreenSide(finger.ScreenPosition)));
            LeanInput.GetKeyPressedStream(Key.LeftArrow).Subscribe(_ => OnJump(-1));
            LeanInput.GetKeyPressedStream(Key.RightArrow).Subscribe(_ => OnJump(1));
        }

        private int ToScreenSide(Vector2 screenPosition)
        {
            var normalisedPosX = screenPosition.x / Screen.width;
            return normalisedPosX < .5f ? -1 : 1;
        }

        private void OnJump(int side)
        {
            if(!_began)
                Begin();
            
            _player.Jump(side);
        }

        public void Begin()
        {
            _began = true;
            _player.ObstacleHitStream.Subscribe(OnLost);
        }

        private void OnLost()
        {
            _began = false;
            _level.Reset();
            _player.Reset();
            _camera.Reset();
        }

        private void Update() => Tick();
        public void Tick()
        {
            if (!_camera.ViewRect.Contains(_player.Rect))
            {
                OnLost();
            }
            
            if (_began)
            {
                _player.Tick();
                _level.Tick();
            }
        }

        private void FixedUpdate() => FixedTick();
        public void FixedTick()
        {
            if (_began)
            {
                _camera.FixedTick();
            }
        }
    }
}