using UnityEngine;

namespace Funko
{
    public class Obstacle : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _leftPart;
        [SerializeField] private SpriteRenderer _rightPart;
        
        public float PosY => transform.position.y;
        
        public void Setup(float posY, float gapNormalisedPosition, float gapWidth)
        {
            var width = FunkoSettingsRef.Value.LevelWidth - FunkoSettingsRef.Value.LevelObstacleGapWidth - 2 * FunkoSettingsRef.Value.LevelObstacleMinWidth;
            var leftWidth = gapNormalisedPosition * width + FunkoSettingsRef.Value.LevelObstacleMinWidth;
            var rightWidth = (1 - gapNormalisedPosition) * width + FunkoSettingsRef.Value.LevelObstacleMinWidth;
            
            var leftPos = -.5f * FunkoSettingsRef.Value.LevelWidth + .5f * leftWidth;
            var rightPos = .5f * FunkoSettingsRef.Value.LevelWidth - .5f * rightWidth;
            
            transform.position = Vector2.up * posY;
            _leftPart.size = new Vector2(leftWidth, 1);
            _leftPart.transform.localPosition = Vector2.right * leftPos;
            _rightPart.size = new Vector2(rightWidth, 1);
            _rightPart.transform.localPosition = Vector2.right * rightPos;
        }
    }
}