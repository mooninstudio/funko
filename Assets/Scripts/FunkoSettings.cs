using UnityEngine;

namespace Funko
{
    [CreateAssetMenu(
        fileName = nameof(FunkoSettings), 
        menuName = ScriptableObjectConsts.ProjectPrefix + nameof(FunkoSettings))]
    public class FunkoSettings : ScriptableObject
    {
        [Header("Level")]
        [SerializeField] private float _levelWidth;
        [SerializeField] private float _levelObstacleSpacing;
        [SerializeField] private float _levelObstacleMinWidth;
        [SerializeField] private float _levelObstacleGapWidth;
        
        [Header("Camera")]
        [SerializeField] private MinMax _cameraOffset;
        [SerializeField] private float _cameraSpeed;
        [SerializeField] private float _cameraFollowSpeed;

        [Header("Player")]
        [SerializeField] private float _playerHorizontalSpeed;
        [SerializeField] private float _playerJumpHeight;
        [SerializeField] private float _playerTimeToJumpApex;
        [SerializeField] private float _playerMaxVerticalVelocity;
        [SerializeField] private LayerMask _playerCollisionMask;

        //  Level
        public float LevelWidth => _levelWidth;
        public float LevelObstacleSpacing => _levelObstacleSpacing;
        public float LevelObstacleMinWidth => _levelObstacleMinWidth;
        public float LevelObstacleGapWidth => _levelObstacleGapWidth;
        
        //  Camera
        public MinMax CameraOffset => _cameraOffset;
        public float CameraSpeed => _cameraSpeed;
        public float CameraFollowSpeed => _cameraFollowSpeed;
        
        //  Player
        public float PlayerHorizontalSpeed => _playerHorizontalSpeed;
        public float PlayerJumpHeight => _playerJumpHeight;
        public float PlayerTimeToJumpApex => _playerTimeToJumpApex;
        public float PlayerMaxVerticalVelocity => _playerMaxVerticalVelocity;
        public LayerMask PlayerCollisionMask => _playerCollisionMask;
    }
    
    public static class FunkoSettingsRef
    {
        private static FunkoSettings _value;
        public static FunkoSettings Value => _value ?? (_value = Resources.Load<FunkoSettings>(nameof(FunkoSettings)));
    }
}