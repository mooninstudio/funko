using System;
using UniRx;
using UnityEngine;

namespace Funko
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerGraphics _graphics;
        
        public IObservable<Unit> ObstacleHitStream => _obstacleHitSubject.AsObservable();
        public Rect Rect => new(_collider.bounds.min, _collider.bounds.size);

        private Subject<Unit> _obstacleHitSubject = new();
        private Rigidbody2D _rigidbody;
        private Collider2D _collider;
        private Vector2 _velocity;
        private float _jumpVelocity;
        private float _gravity;
        private int _jumpDir;
        private bool _jumpedThisFrame;

        private void Awake()
        {
            _gravity = FunkoSettingsRef.Value.PlayerJumpHeight / (FunkoSettingsRef.Value.PlayerTimeToJumpApex.Sqr() * .5f);
            _jumpVelocity = _gravity * FunkoSettingsRef.Value.PlayerTimeToJumpApex;
            _rigidbody = GetComponent<Rigidbody2D>();
            _rigidbody.gravityScale = 0;
            _rigidbody.freezeRotation = true;
            _collider = GetComponent<Collider2D>();
            _jumpDir = -1;
        }

        public void Jump(int side)
        {
            _jumpedThisFrame = true;
            _jumpDir = side;
            _graphics.OnJump();
        }
        
        public void Tick()
        {
            _velocity = CalculateVelocity();
            _rigidbody.velocity = _velocity;
            _graphics.Tick(_velocity.y);
            
            ConsumeInput();
        }

        private Vector2 CalculateVelocity()
        {
            var newVelocity = _jumpedThisFrame ?
                new Vector2(_jumpDir * FunkoSettingsRef.Value.PlayerHorizontalSpeed, _jumpVelocity) :
                _velocity + Vector2.down * _gravity * Time.deltaTime;

            newVelocity.y = newVelocity.y.Sign() * 
                            newVelocity.y.Abs().Max(FunkoSettingsRef.Value.PlayerMaxVerticalVelocity);
            return newVelocity;
        }

        private void ConsumeInput()
        {
            _jumpedThisFrame = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.IsInLayerMask(FunkoSettingsRef.Value.PlayerCollisionMask))
                OnObstacleHit();
        }

        private void OnObstacleHit()
        {
            _obstacleHitSubject.OnNext();
        }
        
        public void Reset()
        {
            ConsumeInput();
            _rigidbody.velocity = default;
            _velocity = default;
            transform.position = default;
            _graphics.Reset();
        }
    }
}
