using System;
using UnityEngine;

namespace Funko
{
    public class PlayerGraphics : MonoBehaviour
    {
        [SerializeField, Range(0, 1)] private float _velocitySquash;
        [SerializeField, Range(0, 1)] private float _jumpSquash;
        [SerializeField] private float _squashSpeed;

        private float _targetSquash;
        private float _squash;
        
        public void Tick(float velocity)
        {
            _targetSquash = velocity.Abs().Map(0, FunkoSettingsRef.Value.PlayerMaxVerticalVelocity, 0, _velocitySquash); 
            SetSquash(Mathf.MoveTowards(_squash, _targetSquash, _squashSpeed * Time.deltaTime));
        }

        private void SetSquash(float squash)
        {
            _squash = squash;
            transform.localScale = new Vector2(1 - squash, 1 + squash);
        }

        public void OnJump()
        {
            SetSquash(-_jumpSquash);
        }

        public void Reset()
        {
            SetSquash(0);
            _targetSquash = 0;
        }
    }
}