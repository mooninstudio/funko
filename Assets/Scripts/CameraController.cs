using System;
using RectEx;
using UnityEngine;

namespace Funko
{
    public class CameraController : CameraControllerBase
    {
        [SerializeField] private MeshRenderer _renderer;
        
        private Transform _target;
        
        
        public void SetTarget(Transform target)
        {
            _target = target;
            Reset();
        }

        public void FixedTick()
        {
            if (!_target)
                return;

            var offset = Center.y - _target.position.y;
            var velocity = offset.Map(
                FunkoSettingsRef.Value.CameraOffset.min,
                FunkoSettingsRef.Value.CameraOffset.max,
                FunkoSettingsRef.Value.PlayerMaxVerticalVelocity,
                FunkoSettingsRef.Value.CameraSpeed);

            SetCenter(Center.y + velocity * Time.deltaTime);
        }

        public void Reset()
        {
            SetCenter(_target.position.y + FunkoSettingsRef.Value.CameraOffset.max);
        }

        public void SetCenter(float y) => SetCenter(new Vector3(0, y, -10));
        public override void SetCenter(Vector3 center)
        {
            base.SetCenter(center);
            ScrollBackground();
        }
        
        private void ScrollBackground()
        {
            _renderer.material.mainTextureOffset = Vector2.up * Center.y * .1f;
        }
    }
}