using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Funko
{
    public class Level : MonoBehaviour
    {
        [SerializeField] private Obstacle _obstaclePrefab;

        private Queue<Obstacle> _obstacles = new();
        private Transform _target;
        private float _fovHeight;

        public void Setup(Transform target, float fovHeight)
        {
            _target = target;
            _fovHeight = fovHeight;
        }

        public void Tick()
        {
            var lastObstacle = _obstacles.LastOrDefault(); 
            if (lastObstacle && lastObstacle.PosY > _target.position.y + .5f * _fovHeight + FunkoSettingsRef.Value.CameraOffset.max)
                return;
            
            GenerateNewObstacle(lastObstacle);
        }

        private void GenerateNewObstacle(Obstacle last)
        {
            if (_obstacles.TryPeek(out var obstacle) && obstacle.PosY < _target.position.y - .5f * _fovHeight + FunkoSettingsRef.Value.CameraOffset.max)
                obstacle = _obstacles.Dequeue();
            else
                obstacle = Instantiate(_obstaclePrefab, transform);
            
            obstacle.Setup(
                last ? last.PosY + FunkoSettingsRef.Value.LevelObstacleSpacing : _target.position.y + .5f * _fovHeight + FunkoSettingsRef.Value.CameraOffset.max,
                Random.value,
                FunkoSettingsRef.Value.LevelObstacleGapWidth);
            
            _obstacles.Enqueue(obstacle);
        }

        public void Reset()
        {
            _obstacles.ForEach(o => Destroy(o.gameObject));
            _obstacles.Clear();
        }
    }
}